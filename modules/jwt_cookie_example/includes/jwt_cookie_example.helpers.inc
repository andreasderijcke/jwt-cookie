<?php
/**
 * @file
 * Helper functions.
 */

/**
 * Create custom block markup.
 *
 * @return string
 *   Block content HTML.
 */
function _jwt_cookie_example_block_user_info() {

  // Default user image.
  $img = file_create_url('module://jwt_cookie_example/images/icon.png');

  // Anonymous section.
  $markup_anonymous = <<<MARKUP
<div class="%s">
  <a href="/user" class="user-info" title="%s">
    <img src="%s" alt="%s" />
    <span class="user-name">%s</span>
  </a>
</div>
MARKUP;

  $text = t('Login');
  $output = sprintf($markup_anonymous, 'jwt-default', $text, $img, $text, $text);

  // Logged-in users section with replacement class and data-jwt attributes.
  $markup_loggedin = <<<MARKUP
<div class="%s">
  <a href="/user" class="jwt-replace user-info" data-jwt="user-url" title="%s">
    <img class="jwt-replace" data-jwt="user-img" src="%s" alt="%s" />
    <span class="jwt-replace user-name" data-jwt="user-name">%s</span>
  </a>
  <a href="/user/logout" class="user-logout" title="%s">%s</a>
</div>
MARKUP;
  $text = t('My account');
  $text_logout = t('Log out');
  $output .= sprintf($markup_loggedin, 'jwt-user', $text, $img, $text, $text, $text_logout, $text_logout);

  return $output;
}

/**
 * Add data to JWT cookie.
 *
 * @param array $data
 *   JWT data array.
 *
 * @see hook_jwt_cookie_data_alter()
 */
function _jwt_cookie_example_jwt_cookie_data_alter(&$data) {
  $user = $GLOBALS['user'];
  $attributes = array();
  $markup = array();

  // Add user profile url.
  $attributes['user-url'] = array('href' => '/user/' . $user->uid);

  // Add user picture, if set.
  if ($user->picture) {
    $user = user_load($user->uid);
    $user_img = array(
      'alt' => $user->name,
      'src' => image_style_url('icon', $user->picture->uri),
    );
    $attributes['user-img'] = $user_img;
  }

  // Add username.
  $markup['user-name'] = $user->name;

  // Merge into $data.
  jwt_cookie_cookie_data_add($data, $attributes, $markup);
}

/**
 * Update JWT user data on user edit.
 *
 * @param array $edit
 *   The edited data array.
 * @param object $user
 *   The updated user object.
 *
 * @see jwt_cookie_cookie_data_update()
 */
function _jwt_cookie_example_jwt_cookie_cookie_data_update($edit, $user) {

  $attributes = array();
  $markup = array();

  // Username updated.
  if ($edit['original']->name !== $edit['name']) {
    // Update username markup.
    $markup['user-name'] = $edit['name'];

    // Update user picture alt.
    $attributes['user-img']['alt'] = $edit['name'];
  }

  // Clear picture if deleted.
  if ($edit['picture_delete']) {
    $attributes['user-img']['src'] = FALSE;
  }

  if ($edit['picture_upload']) {
    $attributes['user-img']['src'] = image_style_url('icon', $edit['picture']->uri);
  }

  jwt_cookie_cookie_data_update($attributes, $markup);
}
