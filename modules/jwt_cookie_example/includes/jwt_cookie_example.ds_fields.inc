<?php
/**
 * @file
 * Custom DS fields.
 */

/**
 * Field showing current date time.
 */
function _jwt_cookie_example_ds_current_datetime() {

  return date('D, d M Y H:i:s');
}
