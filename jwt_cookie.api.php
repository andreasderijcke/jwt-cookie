<?php
/**
 * @file
 * Hooks provided by JWT cookie module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Implements hook_jwt_cookie_data_alter().
 *
 * @see jwt_cookie_example.helpers.inc
 */
function hook_jwt_cookie_data_alter(&$data) {
  $user = $GLOBALS['user'];
  $attributes = array();
  $markup = array();

  // Add user profile url.
  $attributes['user-url'] = array('href' => '/user/' . $user->uid);

  // Add user picture, if set.
  if ($user->picture) {
    $user = user_load($user->uid);
    $user_img = array(
      'alt' => $user->name,
      'src' => image_style_url('icon', $user->picture->uri),
    );
    $attributes['user-img'] = $user_img;
  }

  // Add username.
  $markup['user-name'] = $user->name;

  // Merge into $data.
  jwt_cookie_cookie_data_add($data, $attributes, $markup);
}
