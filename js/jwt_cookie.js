/*
 * Handle JWT replacements.
 */
(function ($) {

  "use strict";

  Drupal.behaviors.jwtCookie = {
    attach: function (context, settings) {

      // Only process DOM elements (for now)
      if (context.nodeType) {

        var token = Drupal.behaviors.jwtCookieGetToken();
        if (token) {
          var data = JSON.parse(token['data']);

          $('.jwt-replace', context).once('jwt-processed').each(function () {
            var tag = $(this).data('jwt');

            if (data.attr[tag]) {
              $(this).attr(data.attr[tag]);
            }
            if (data.html[tag]) {
              $(this).html(data.html[tag]);
            }
          });

          $('body', context).addClass('jwt-loggedin').removeClass('jwt-anon');
        }
      }
    }
  };

  Drupal.behaviors.jwtCookieGetToken = function () {
    var token = $.cookie('jwt_cookie');

    if (token) {
      var base64Url = token.split('.')[1];
      var rem = base64Url.length % 4;
      if (rem) {
        base64Url = (base64Url + '====').slice(0, base64Url.length + 4 - rem);
      }
      return JSON.parse(window.atob(base64Url.replace(/-/g, '+').replace(/_/g, '/')));
    }

    return false;
  };
})(jQuery);