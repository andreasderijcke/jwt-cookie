<?php
/**
 * @file
 * Helper functions.
 */

/**
 * Get cached JWT data for user.
 *
 * @param int $uid
 *   The user id, used as 'cid'.
 *
 * @return object|FALSE
 *   Data object or FALSE if nothing present.
 */
function _jwt_cookie_cache_get($uid) {
  $cache = cache_get($uid, $bin = 'cache_jwt_cookie');

  return $cache ? $cache->data : array();
}

/**
 * Cache JWT data for user.
 *
 * @param int $uid
 *   The user id, used as 'cid'.
 * @param array $data
 *   Data to store.
 */
function _jwt_cookie_cache_set($uid, $data) {
  cache_set($uid, $data, 'cache_jwt_cookie', CACHE_PERMANENT);
}

/**
 * Clean a value for JWT compatibility.
 *
 * @param string $value
 *   The value to clean.
 *
 * @return string
 *   The cleaned value.
 */
function _jwt_cookie_clean_value($value) {
  // Strip out line breaks (from tpls, markup).
  $value = preg_replace('/\n\s*/', '', $value);
  // Replace multiple spaces with 1.
  $value = preg_replace('/\s{2,}/', ' ', $value);
  // Force UTF-8, this fixes some decoding issues in JS.
  $value = mb_convert_encoding($value, "UTF-8");

  return $value;
}

/**
 * Get active domain.
 *
 * @return string
 *    The domain.
 */
function _jwt_cookie_get_domain() {
  return $_SERVER['HTTP_HOST'];
}

/**
 * Fetch the signature key.
 *
 * @return string
 *    The key value
 */
function _jwt_cookie_get_signature_key() {
  $settings = _jwt_cookie_settings_get();
  $signature = JWT_DEFAULT_SIGNATURE;

  // Get signature key.
  $key_id = $settings['jwt_cookie_key'];
  $value = key_get_key_value($key_id);
  $signature = $value ? $value : $signature;

  return $signature;
}
