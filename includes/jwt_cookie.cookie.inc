<?php
/**
 * @file
 * All cookie related actions.
 */

module_load_include('inc', 'jwt_cookie', 'includes/jwt_cookie.helpers');

include __DIR__ . '/../vendor/autoload.php';

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;

/**
 * Create cookie with given data.
 *
 * @param object $user
 *    The user.
 * @param array $data
 *   The data to set.
 * @param bool $cached
 *   Flag to indicate we are passing data from cache, to skip data cleaning and
 *   re-saving the data to cache.
 *
 * @return bool
 *   TRUE on success.
 */
function _jwt_cookie_cookie_create($user, $data, $cached = FALSE) {
  $cookie_domain = _jwt_cookie_get_domain();
  $builder = new Builder();
  $signer = new Sha256();

  // Build the token.
  $expiration = time() + ini_get('session.cookie_lifetime');
  $builder
    ->setIssuer($cookie_domain)
    ->setId(session_id(), TRUE)
    ->setIssuedAt(time())
    ->setExpiration($expiration)
    ->set('uid', $user->uid)
    ->set('roles', implode(',', $user->roles));

  // When we use data from cache, we don't need to clean the data again.
  if (!$cached) {
    // Get and clean the data. Discard the rest.
    $attr = isset($data['attr']) ? $data['attr'] : array();
    $html = isset($data['html']) ? $data['html'] : array();
    unset($data);

    foreach ($html as $tag => $value) {
      $html[$tag] = _jwt_cookie_clean_value($value);
    }
    $data['html'] = $html;

    foreach ($attr as $tag => $attributes) {
      foreach ($attributes as $attribute => $value) {
        $attributes[$attribute] = _jwt_cookie_clean_value($value);
      }
    }
    $data['attr'] = $attr;

    // Store in cache.
    _jwt_cookie_cache_set($user->uid, $data);
  }

  $data = json_encode($data);
  $builder->set('data', $data);

  // Wrap up and set the cookie.
  $signature = _jwt_cookie_get_signature_key();
  $builder->sign($signer, $signature);
  $token = $builder->getToken();

  return setcookie(JWT_COOKIE_NAME, $token, $expiration, '/', '.' . $cookie_domain);
}

/**
 * Add data to the cookie.
 *
 * To be used in hook implementing hook_jwt_cookie_data_alter().
 *
 * @param array $data
 *   The cookie data array.
 * @param array $attributes
 *   The attributes to add/update/remove.
 *   E.g.:
 *   array(
 *      'user-img => array(
 *          'src' => '',
 *          'alt' => '',
 *       ),
 *   );
 * @param array $markup
 *   The markup to add/update/remove.
 *   E.g.:
 *   array(
 *      'user-name => '',
 *   );
 */
function _jwt_cookie_cookie_data_add(&$data, $attributes = array(), $markup = array()) {

  // Process attributes.
  if ($attributes) {

    foreach ($attributes as $tag => $item) {

      if (isset($data['attr'][$tag])) {
        $data['attr'][$tag] = array_merge($data['attr'][$tag], $item);
      }
      else {
        $data['attr'][$tag] = $item;
      }
    }
  }

  // Process markup.
  if ($markup) {
    if (!isset($data['html'])) {
      $data['html'] = array();
    }
    $data['html'] = array_merge($data['html'], $markup);
  }
}

/**
 * Update cookie attribute data.
 *
 * @param array $attributes
 *   The attributes to add/update/remove.
 * @param array $markup
 *   The markup to add/update/remove.
 */
function _jwt_cookie_cookie_data_update($attributes, $markup) {
  $user  = $GLOBALS['user'];

  // If we have cached data, use it.
  $cache = _jwt_cookie_cache_get($user->uid);

  // Process attributes.
  if ($attributes) {

    foreach ($attributes as $tag => $item) {

      if (!empty($item)) {

        foreach ($item as $attribute => $value) {
          // Remove empty attribute values.
          if (empty($value)) {
            unset($cache['attr'][$tag][$attribute]);
          }
          else {
            $cache['attr'][$tag][$attribute] = $value;
          }
        }
      }
      else {
        unset($cache['attr'][$tag]);
      }
    }
  }

  // Process markup.
  if ($markup) {

    foreach ($markup as $tag => $value) {

      if (!empty($item)) {
        $cache['html'][$tag] = $value;
      }
      else {
        unset($cache['html'][$tag]);
      }
    }
  }

  // Create new cookie.
  _jwt_cookie_cookie_create($user, $cache);
}

/**
 * Delete JWT cookie for user.
 */
function _jwt_cookie_cookie_delete() {

  $cookie_domain = _jwt_cookie_get_domain();
  setcookie(JWT_COOKIE_NAME, '', 1, '/', '.' . $cookie_domain);

  unset($_COOKIE[JWT_COOKIE_NAME]);
}

/**
 * Get JWT token.
 *
 * @return bool|\Lcobucci\JWT\Token
 *   FALSE if cookie not set, \Lcobucci\JWT\Token if set.
 */
function _jwt_cookie_cookie_get_token() {
  // Get current cookie, unpack.
  if (isset($_COOKIE['jwt_cookie'])) {

    return (new Parser())->parse((string) $_COOKIE['jwt_cookie']);
  }

  return FALSE;
}

/**
 * Set a JWT cookie for user.
 *
 * @param object $user
 *   The user logging in.
 *
 * @return bool
 *   TRUE on success.
 */
function _jwt_cookie_cookie_set($user) {

  // If we have cached data, use it.
  $data = _jwt_cookie_cache_get($user->uid);
  $cached = TRUE;

  if (!$data) {
    // Allow other modules to add data.
    drupal_alter('jwt_cookie_data', $data);
    $cached = FALSE;
  }

  // Create it.
  return _jwt_cookie_cookie_create($user, $data, $cached);
}

/**
 * Validate cookie token.
 *
 * @param string $token
 *   The JWT cookie token to validate.
 *
 * @return bool
 *   TRUE on valid token.
 */
function _jwt_cookie_cookie_validate($token) {

  // Check if tampered by signature check.
  $signer = new Sha256();
  return $token->verify($signer, _jwt_cookie_get_signature_key());
}
