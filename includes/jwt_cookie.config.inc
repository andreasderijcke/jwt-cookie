<?php
/**
 * @file
 * Module settings.
 */

module_load_include('inc', 'jwt_cookie', 'includes/jwt_cookie.settings');

/**
 * Settings page form.
 */
function jwt_cookie_settings($form, &$form_state) {

  $settings = _jwt_cookie_settings_get();

  $form['jwt_cookie_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable JWT mode'),
    '#required' => FALSE,
    '#default_value' => isset($settings['jwt_cookie_enabled']) ? $settings['jwt_cookie_enabled'] : 0,
  );

  $form['jwt_cookie_key'] = [
    '#type' => 'key_select',
    '#title' => t('Signature key'),
    '#default_value' => isset($settings['jwt_cookie_key']) ? $settings['jwt_cookie_key'] : '',
  ];

  // Form Submit.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submit handler for jwt_cookie_settings().
 */
function jwt_cookie_settings_submit($form, &$form_state) {
  form_state_values_clean($form_state);

  _jwt_cookie_settings_save($form_state['values']);

}
