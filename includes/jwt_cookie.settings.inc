<?php
/**
 * @file
 * Setting functions.
 */

/**
 * Get settings.
 *
 * @return bool
 *   TRUE if enabled.
 */
function _jwt_cookie_settings_get() {
  $cache = &drupal_static('jwt_cookie_settings');

  if ($cache) {
    return $cache;
  }

  $cache = variable_get('jwt_cookie_settings', FALSE);

  return $cache;
}

/**
 * Save settings.
 *
 * @param array $settings
 *   Settings to stave.
 */
function _jwt_cookie_settings_save($settings) {

  variable_set('jwt_cookie_settings', $settings);
  $cache = &drupal_static('jwt_cookie_settings');
  $cache = $settings;

  drupal_set_message(t('Settings have been saved.'));
}