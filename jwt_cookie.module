<?php
/**
 * @file
 * JSON Web Token cookie.
 */

define('JWT_COOKIE_NAME', 'jwt_cookie');
define('JWT_DEFAULT_SIGNATURE', '94qsb1fpluinp129mxzk4rav0m3ntcvseo4xogv1io5nur893ddvzf9ipwfgqt02');

/**
 * Implements hook_init().
 */
function jwt_cookie_init() {

  if (jwt_cookie_is_enabled()) {
    $logged_in = user_is_logged_in();

    // Remove JWT cookie if user is not logged in.
    if (!$logged_in && isset($_COOKIE['jwt_cookie'])) {
      module_load_include('inc', 'jwt_cookie', 'includes/jwt_cookie.cookie');
      _jwt_cookie_cookie_delete();
    }

    // Make sure a logged in user has a valid token.
    if ($logged_in) {
      module_load_include('inc', 'jwt_cookie', 'includes/jwt_cookie.cookie');

      $token = _jwt_cookie_cookie_get_token();@
      $valid = ($token) ? _jwt_cookie_cookie_validate($token) : FALSE;

      // Create token if not present or invalid.
      if (!$valid) {
        _jwt_cookie_cookie_set($GLOBALS['user']);
      }
    }
  }
}
/**
 * Implements hook_page_build().
 *
 * Using this hook instead of hook_init(), as we want to skip this from being
 * called on AJAX calls etc.
 */
function jwt_cookie_page_build(&$page) {

  $path_is_admin = path_is_admin(current_path());
  // Only for front, or edit pages with frontend theme.
  if (jwt_cookie_is_enabled() && !$path_is_admin || $path_is_admin && !user_access('view the administration theme', $GLOBALS['user'])) {

    // Add required JS and CSS.
    drupal_add_library('system', 'jquery.cookie', TRUE);

    /*
     * JS for replacing content from the cookie, weight needs to be low so it
     * executes for other JS adds click handlers and other stuff (like flag.js).
     */
    $js_options = array(
      'type' => 'file',
      'group' => JS_DEFAULT,
      'every_page' => TRUE,
      'weight' => -20,
    );
    drupal_add_js(drupal_get_path('module', 'jwt_cookie') . '/js/jwt_cookie.js', $js_options);

    /*
     * CSS also as low as possible.
     */
    $css_options = array(
      'type' => 'file',
      'group' => CSS_DEFAULT,
      'every_page' => TRUE,
      'weight' => -20,
    );
    drupal_add_css(drupal_get_path('module', 'jwt_cookie') . '/css/jwt_cookie.css', $css_options);
  }
}

/**
 * Implements hook_menu().
 */
function jwt_cookie_menu() {
  $items = array();

  $items['admin/config/system/jwt_cookie'] = array(
    'title' => 'JWT cookie',
    'description' => 'Manage JSON Web Token cookie support.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('jwt_cookie_settings'),
    'access arguments' => array('access jwt_cookie settings'),
    'file' => 'includes/jwt_cookie.config.inc',
  );

  return $items;
}

/**
 * Implements hook_preprocess_html().
 */
function jwt_cookie_preprocess_html(&$variables) {

  if (jwt_cookie_is_enabled()) {

    // Add default classes.
    $variables['classes_array'][] = 'jwt-enabled';
    $variables['classes_array'][] = 'jwt-anon';

    // TODO; strip class 'logged-in'? Should be for pages in Varnish, but not
    // for the others... :/
  }
}

/**
 * Implements hook_user_login().
 */
function jwt_cookie_user_login(&$edit, $account) {

  if (jwt_cookie_is_enabled()) {
    module_load_include('inc', 'jwt_cookie', 'includes/jwt_cookie.cookie');
    _jwt_cookie_cookie_set($account);
  }
}

/**
 * Implements hook_user_logout().
 */
function jwt_cookie_user_logout($account) {

  if (jwt_cookie_is_enabled()) {
    module_load_include('inc', 'jwt_cookie', 'includes/jwt_cookie.cookie');
    _jwt_cookie_cookie_delete();
  }
}

/*
 * Public functions.
 */

/**
 * Test if JWT mode is enabled.
 *
 * @return bool
 *   TRUE if enabled.
 */
function jwt_cookie_is_enabled() {

  module_load_include('inc', 'jwt_cookie', 'includes/jwt_cookie.settings');

  $settings = _jwt_cookie_settings_get();
  return isset($settings['jwt_cookie_enabled']) ? (bool) $settings['jwt_cookie_enabled'] : FALSE;
}

/**
 * Add data to the cookie.
 *
 * To be used in hook implementing hook_jwt_cookie_data_alter().
 *
 * @param array $data
 *   The cookie data array.
 * @param array $attributes
 *   The attributes to add/update/remove.
 *   E.g.:
 *   array(
 *      'user-img => array(
 *          'src' => '',
 *          'alt' => '',
 *       ),
 *   );
 * @param array $markup
 *   The markup to add/update/remove.
 *   E.g.:
 *   array(
 *      'user-name => '',
 *   );
 */
function jwt_cookie_cookie_data_add(&$data, $attributes = array(), $markup = array()) {

  module_load_include('inc', 'jwt_cookie', 'includes/jwt_cookie.cookie');
  _jwt_cookie_cookie_data_add($data, $attributes, $markup);
}

/**
 * Update data in the cookie, forcing the cookie to be recreated.
 *
 * Use empty values to remove data from the cookie.
 *
 * @param array $attributes
 *   The attributes to add/update/remove.
 *   E.g.:
 *   array(
 *      'user-img => array(
 *          'src' => '',
 *          'alt' => '',
 *       ),
 *   );
 * @param array $markup
 *   The markup to add/update/remove.
 *   E.g.:
 *   array(
 *      'user-name => '',
 *   );
 */
function jwt_cookie_cookie_data_update($attributes = array(), $markup = array()) {

  module_load_include('inc', 'jwt_cookie', 'includes/jwt_cookie.cookie');
  _jwt_cookie_cookie_data_update($attributes, $markup);
}

/**
 * Adds 'Vary: X-login' login header to the page.
 *
 * To be used when a page, node, entity requires the user to be authenticated
 * to be viewed. This header will allow Varnish to handle this for us.
 */
function jwt_cookie_require_cookie_authentication() {
  drupal_add_http_header('Vary', 'X-login');
}
