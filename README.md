# About

This module implements the use of JSON Web Token cookies. The main
reason is to have Varnish cache most pages, and still allow us to show
user specific data on those pages.

The goal of this module is to provide generic JWT support/API to allow
easy conversion of other (custom) modules for JWT support.

This module provides:

1. Creation, deletion, updating of the JWT cookie.
1. Frontend content replacements on page load with data from the cookie.
1. Show user specific data when JWT cookie is present.
1. Toggle between normal and 'JWT mode'.
1. Set JWT signature key using 'key' module.

## Default JWT cookie content

Currently, following data is stored by default in the cookie:

1. jit: Session value
1. iat: creation timestamp
1. exp: expiration timestamp
1. uid: User ID


## JWT cookie lifetime

_session.cookie_lifetime_ variable is used.


# How to use in other modules

## Principles

In 'JWT mode', JWT supporting modules should:

1. Output both anonymous and logged-in user markup, each in their own
   wrapper.
   
   Anonymous markup wrapper must have class __jwt-default__.  
   Logged-in user markup wrapper must class __jwt-user__.  
   
   Visibility will be toggled based on these classes.
   
1. Anonymous markup should contain the markup as normally present to
   anonymous users.
   
1. Logged-in user markup should be the same markup as presented to the
   the logged-in user, with the following additions:
   
   1. Each element requiring a replacement, either an element attribute or content, require class __jwt-replace__ and
      attribute __data-jwt="[your-claim]"__.
      
   1. __[your-claim]__ as used above should be a short but unique identifier
      for that specific piece of data. This can be anything but one of the
      Registered Claim Names for JWT, see [RFC7519#section-4.1](https://tools.ietf.org/html/rfc7519#section-4.1).
      This list as for now is: iss, sub, aud, exp, nbf, iat, jti.
      
   1. Use appropriate defaults for the values to be replaced. If no replacement is available, these defaults will be used. 
   
1. Add custom data to the token:
      
    1. Add data for __[your-claim]__ by implementing _hook_jwt_cookie_data_alter()_.
      
    1. You can add data to the token either as markup or attributes:
    
       1.  Adding markup will replace the contents of the element
       with the corresponding data-jwt value.  
       For example: contents of an _a_ element.
       
       1.  Adding attributes will allow you to replace multiple attributes on the corresponding element.  
       For example: _src_ and _alt_ of an _img_ element.
       
       1. Also, both markup and attributes can be added for the same element.  
       For example: content, _href_ and _title_ of an _a_ element.
    
       1. Only add markup or attribute data when it is different from the default markup (keep cookie as small as possible).
       
       1. As we cannot test for setcookie() to succeed, be smart about what data you add and keep it as minimal as possible.

Following these principles, all markup replacements will be taken care off on page load.

See jwt_cookie_example.helpers.inc for an example, both for adding data to the token as for the corresponding markup.


## Important notes

In 'JWT mode':

1. The body will have the class 'jwt-enabled'.
1. The body will also have the class 'jwt-anon' by default.
1. When the JWT cookie is processed, 'jwt-anon' will be replaced with 'jwt-loggedin'.
1. The cookie size should be limited to 6-7kb, so try to minimize content per claim.
   We have currently no way of detecting if setcookie succeeds.
