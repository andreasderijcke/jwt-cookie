'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

/**
 * Launch the Server
 */
 gulp.task('sass', function () {
   return gulp.src('./scss/**/*.scss')
   .pipe(sourcemaps.init())
   .pipe(sass().on('error', sass.logError))
   .pipe(autoprefixer()) // Right now, we run with the default configuration from Autoprefixer which is
   // Browsers with over 1% market share,
   // Last 2 versions of all browsers,
   // Firefox ESR,
   // Opera 12.14
   .pipe(cleanCSS())
   .pipe(sourcemaps.write('./../css'))
   .pipe(gulp.dest('./css'))
 });



 gulp.task('default', function () {
   gulp.watch('./scss/**/*.scss', ['sass']);
 });
